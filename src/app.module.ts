import { Module } from '@nestjs/common';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ErrorInterceptors } from './interceptors/errors.interceptor';
import { MedicallicensesModule } from './medicallicenses/medicallicenses.module';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [MedicallicensesModule, ConfigModule.forRoot({ isGlobal: true })],
  controllers: [AppController],
  providers: [
    AppService,
    { provide: APP_INTERCEPTOR, useClass: ErrorInterceptors },
  ],
})
export class AppModule {}
