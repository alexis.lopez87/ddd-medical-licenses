import { Body, Controller, Get, Logger, Param, Post } from '@nestjs/common';
import { AffiliateUseCases } from './use-cases/affiliate.use-case';
import { UnmarshalledAffiliate } from './domain/affiliate';
import { UnmarshalledMedicalLicense } from './domain/medical-license';
@Controller('medicallicenses')
export class MedicallicensesController {
  private readonly logger = new Logger(MedicallicensesController.name);

  constructor(private readonly affiliateUseCases: AffiliateUseCases) {}

  @Post('create')
  async create(@Body() dtoMedicalLicense: UnmarshalledMedicalLicense) {
    return await this.affiliateUseCases.insertMedicalLicense(dtoMedicalLicense);
  }

  @Get('affiliate/:id/licenses')
  async getLicensesByAffiliateId(@Param('id') id: string) {
    return await this.affiliateUseCases.getLicensesByAffiliate(id);
  }

  @Get('affiliate/:id')
  async getByAffiliateId(@Param('id') id: string) {
    return await this.affiliateUseCases.getById(id);
  }

  @Post('affiliate')
  async createAffiliate(@Body() dtoAffiliate: UnmarshalledAffiliate) {
    return await this.affiliateUseCases.create(dtoAffiliate);
  }

  @Get('affiliates')
  async getByAffiliates() {
    return await this.affiliateUseCases.findAll();
  }
}
