import { ApiProperty } from '@nestjs/swagger';
import { Entity } from './entity';
import { IsNotEmpty } from 'class-validator';

export class UnmarshalledMedicalLicense {
  @ApiProperty()
  id?: string;
  @ApiProperty()
  @IsNotEmpty()
  initialDate: Date;
  @ApiProperty()
  @IsNotEmpty()
  endDate: Date;
  @ApiProperty()
  @IsNotEmpty()
  affiliateId: string;
}

interface MedicalLicenseProps {
  id?: string;
  initialDate: Date;
  endDate: Date;
  affiliateId: string;
}

export class MedicalLicense extends Entity<MedicalLicenseProps> {
  private constructor({ id, ...data }: MedicalLicenseProps) {
    super(data, id);
  }

  public static create(props: MedicalLicenseProps): MedicalLicense {
    const instance = new MedicalLicense(props);
    return instance;
  }

  public unmarshal(): UnmarshalledMedicalLicense {
    return {
      id: this.id,
      initialDate: this.initialDate,
      endDate: this.endDate,
      affiliateId: this.affiliateId,
    };
  }

  get id(): string {
    return this._id;
  }

  get initialDate(): Date {
    return this.props.initialDate;
  }

  get endDate(): Date {
    return this.props.endDate;
  }

  get affiliateId(): string {
    return this.props.affiliateId;
  }
}
