export interface MessagesService {
  sendMessage<T>(topicName: string, kind: string, payload: T): Promise<void>;
}
