import { Affiliate } from './affiliate';
import { MedicalLicense } from './medical-license';

export interface AffiliateRepository {
  getById(id: string): Promise<Affiliate>;
  create(affiliate: Affiliate): Promise<Affiliate>;
  update(affiliate: Affiliate): Promise<Affiliate>;
  medicalLicensesByAffiliate(affiliateId: string): Promise<MedicalLicense[]>;
  findAll(): Promise<Partial<Affiliate>[]>;
  insertMedicalLicense(medicalLicense: MedicalLicense): Promise<MedicalLicense>;
}
