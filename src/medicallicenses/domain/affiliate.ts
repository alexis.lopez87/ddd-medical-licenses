import { ApiProperty } from '@nestjs/swagger';
import { ValidationError } from '../libs/errors';
import { Entity } from './entity';
import { MedicalLicense, UnmarshalledMedicalLicense } from './medical-license';
import { IsNotEmpty } from 'class-validator';

export class UnmarshalledAffiliate {
  @ApiProperty()
  id?: string;
  @ApiProperty()
  @IsNotEmpty()
  name: string;
  medicalLicenses: UnmarshalledMedicalLicense[];
}

interface AffiliateProps {
  id?: string;
  name: string;
  medicalLicenses: MedicalLicense[];
}

export class Affiliate extends Entity<AffiliateProps> {
  private constructor({ id, ...data }: AffiliateProps) {
    super(data, id);
  }

  public static create(props: AffiliateProps): Affiliate {
    const instance = new Affiliate(props);
    instance.medicalLicenses = instance.props.medicalLicenses || [];
    return instance;
  }

  get medicalLicenses(): MedicalLicense[] {
    return this.props.medicalLicenses || [];
  }

  set medicalLicenses(licenses: MedicalLicense[]) {
    this.props.medicalLicenses = [...licenses];
  }

  private static validMedicalLicenseId(id: string) {
    return id !== undefined && id !== '';
  }

  private static validMedicalLicenseDates(initalDate: Date, endDate: Date) {
    return initalDate < endDate;
  }

  public unmarshal(): UnmarshalledAffiliate {
    return {
      id: this.id,
      name: this.props.name,
      medicalLicenses: this.props.medicalLicenses,
    };
  }

  public addMedicalLicense(medicalLicense: MedicalLicense): void {
    if (!Affiliate.validMedicalLicenseId(medicalLicense.id)) {
      throw new ValidationError('Medical License must have a valid id');
    }

    if (
      !Affiliate.validMedicalLicenseDates(
        medicalLicense.initialDate,
        medicalLicense.endDate,
      )
    ) {
      throw new ValidationError('Medical License must have valid dates');
    }

    const index = this.medicalLicenses.findIndex(
      (ml) => ml.id === medicalLicense.id,
    );

    if (index > -1) {
      const licenses = [
        ...this.medicalLicenses.slice(0, index),
        medicalLicense,
        ...this.medicalLicenses.slice(index + 1),
      ];

      this.medicalLicenses = licenses;
    } else {
      this.medicalLicenses = [...this.medicalLicenses, medicalLicense];
    }
  }

  public remove(medicalLicenseId: string): void {
    const medicalLicenses = this.medicalLicenses.filter(
      (medicalLicense) => medicalLicense.id !== medicalLicenseId,
    );
    this.medicalLicenses = medicalLicenses;
  }

  public empty(): void {
    this.medicalLicenses = [];
  }

  get id(): string {
    return this._id;
  }

  get name(): string {
    return this.props.name;
  }
}
