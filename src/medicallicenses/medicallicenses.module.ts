import { Module } from '@nestjs/common';
import { MemoryData } from './infraestructure/database/memory/memory-data';
import { AffiliateRepo } from './infraestructure/database/repositories/affiliate.repository';
import { MedicallicensesController } from './medicallicenses.controller';
import { AffiliateUseCases } from './use-cases/affiliate.use-case';
import { PubSubMessages } from './infraestructure/messages/pubsub.messages';
import { keyAffiliateRepository, keyMessagesService } from './libs/constants';

@Module({
  controllers: [MedicallicensesController],
  providers: [
    AffiliateUseCases,
    {
      provide: keyAffiliateRepository,
      useClass: AffiliateRepo,
    },
    {
      provide: keyMessagesService,
      useClass: PubSubMessages,
    },
    MemoryData,
  ],
})
export class MedicallicensesModule {}
