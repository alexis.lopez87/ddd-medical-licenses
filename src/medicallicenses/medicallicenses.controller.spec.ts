import { Test, TestingModule } from '@nestjs/testing';
import { MedicallicensesController } from './medicallicenses.controller';

describe('MedicallicensesController', () => {
  let controller: MedicallicensesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MedicallicensesController],
    }).compile();

    controller = module.get<MedicallicensesController>(
      MedicallicensesController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
