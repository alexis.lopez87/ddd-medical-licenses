import { PubSub } from '@google-cloud/pubsub';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { MessagesService } from 'src/medicallicenses/domain/messages';

@Injectable()
export class PubSubMessages implements MessagesService {
  constructor(private configService: ConfigService) {}

  private gcpProjId = this.configService.get<string>('GCP_PROJ_ID');
  private gcpCredentials = this.configService.get<string>(
    'GOOGLE_APPLICATION_CREDENTIALS',
  );
  private pubSubClient = new PubSub();

  async sendMessage<T>(
    topicName: string,
    kind: string,
    payload: T,
  ): Promise<void> {
    console.log('gcpProjId', this.gcpProjId);

    const dataBuffer = Buffer.from(JSON.stringify(payload));
    const messageId = await this.pubSubClient
      .topic(topicName)
      .publishMessage({ data: dataBuffer, attributes: { kind } });
    console.log(`Message ${messageId} published.`);
  }
}
