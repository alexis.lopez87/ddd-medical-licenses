import { createId } from '@paralleldrive/cuid2';
import {
  Affiliate,
  UnmarshalledAffiliate,
} from 'src/medicallicenses/domain/affiliate';
import { MedicalLicenseMapper } from './medical-license.mapper';

export class AffiliateMapper {
  public static toDomain(raw: UnmarshalledAffiliate): Affiliate {
    let medicalLicensesDto = [];
    if (raw.medicalLicenses) {
      medicalLicensesDto = raw.medicalLicenses.map((ml) => {
        return MedicalLicenseMapper.toDomain(ml);
      });
    }

    return Affiliate.create({
      id: raw.id || createId(),
      name: raw.name,
      medicalLicenses: medicalLicensesDto,
    });
  }
}
