import { createId } from '@paralleldrive/cuid2';
import {
  UnmarshalledMedicalLicense,
  MedicalLicense,
} from 'src/medicallicenses/domain/medical-license';

export class MedicalLicenseMapper {
  public static toDomain(raw: UnmarshalledMedicalLicense): MedicalLicense {
    return MedicalLicense.create({
      id: raw.id || createId(),
      initialDate: raw.initialDate,
      endDate: raw.endDate,
      affiliateId: raw.affiliateId,
    });
  }
}
