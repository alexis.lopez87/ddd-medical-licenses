import { Injectable } from '@nestjs/common';
import { Affiliate } from 'src/medicallicenses/domain/affiliate';
import { MedicalLicense } from 'src/medicallicenses/domain/medical-license';
import { AffiliateRepository } from 'src/medicallicenses/domain/repository';
import {
  ResourceNotFound,
  ValidationError,
} from 'src/medicallicenses/libs/errors';
import { AffiliateMapper } from '../memory/mappers/affiliate.mapper';
import { MedicalLicenseMapper } from '../memory/mappers/medical-license.mapper';
import { MemoryData } from '../memory/memory-data';

@Injectable()
export class AffiliateRepo implements AffiliateRepository {
  constructor(private readonly database: MemoryData) {}

  async getById(id: string): Promise<Affiliate> {
    const affiliate = await this.database.affiliate.getById<Affiliate>(id);
    if (!affiliate) {
      throw new ResourceNotFound(`Affiliate with id ${id}`, { id });
    }
    return AffiliateMapper.toDomain(affiliate);
  }

  async findAll(): Promise<Affiliate[]> {
    const affiliates = await this.database.affiliate.findAll<Affiliate>();

    return affiliates;
  }

  async create(affiliate: Affiliate): Promise<Affiliate> {
    const affiliates = await this.database.affiliate.findAll<Affiliate>();

    const existsAffiliate = affiliates.filter(
      (af) => af.props.name === affiliate.name,
    );

    if (existsAffiliate.length > 0) {
      throw new ValidationError(
        `affiliate name ${affiliate.name} already exists`,
      );
    }

    const newAffiliate = Affiliate.create({
      name: affiliate.name,
      medicalLicenses: [],
    });

    const inserted = this.database.affiliate.insert(newAffiliate);

    return inserted;
  }

  async update(affiliate: Affiliate): Promise<Affiliate> {
    const dtoAffiliate = affiliate.unmarshal();
    const updated = await this.database.affiliate.update(
      affiliate.id,
      dtoAffiliate,
    );
    return AffiliateMapper.toDomain(updated);
  }

  async medicalLicensesByAffiliate(affiliateId: string) {
    const affiliate = await this.database.affiliate.getById<Affiliate>(
      affiliateId,
    );
    if (!affiliate) {
      throw new ResourceNotFound(`Affiliate with id ${affiliateId}`, {
        affiliateId,
      });
    }

    const licenses = affiliate.medicalLicenses.map((ml) =>
      MedicalLicenseMapper.toDomain(ml),
    );

    return licenses;
  }

  async insertMedicalLicense(
    medicalLicense: MedicalLicense,
  ): Promise<MedicalLicense> {
    const affiliate = await this.database.affiliate.getById<Affiliate>(
      medicalLicense.affiliateId,
    );
    if (!affiliate) {
      throw new ResourceNotFound(
        `Affiliate with id ${medicalLicense.affiliateId}`,
        {
          affiliateId: medicalLicense.affiliateId,
        },
      );
    }

    affiliate.addMedicalLicense(medicalLicense);

    return medicalLicense;
  }
}
