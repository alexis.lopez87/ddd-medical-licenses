import { ConfigService } from '@nestjs/config';

interface Topic {
  topic: string;
  kind: string;
}
export class AffiliateTopic implements Topic {
  constructor(private configService: ConfigService) {}
  topic = this.configService.get<string>('TOPIC');
  kind = 'affiliates';
}
