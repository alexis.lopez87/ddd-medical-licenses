import { Inject, Injectable } from '@nestjs/common';
import { UnmarshalledAffiliate } from '../domain/affiliate';
import { UnmarshalledMedicalLicense } from '../domain/medical-license';
import { AffiliateMapper } from '../infraestructure/database/memory/mappers/affiliate.mapper';
import { MedicalLicenseMapper } from '../infraestructure/database/memory/mappers/medical-license.mapper';
import { AffiliateTopic } from '../libs/topics';
import { MessagesService } from '../domain/messages';
import { AffiliateRepository } from '../domain/repository';
import { keyAffiliateRepository, keyMessagesService } from '../libs/constants';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AffiliateUseCases {
  constructor(
    @Inject(keyAffiliateRepository)
    private readonly affiliateRepo: AffiliateRepository,
    @Inject(keyMessagesService)
    private readonly messagesService: MessagesService,
    private configService: ConfigService,
  ) {}

  public async getById(id: string): Promise<UnmarshalledAffiliate> {
    const affiliate = await this.affiliateRepo.getById(id);
    const affiliateDto = affiliate.unmarshal();
    affiliateDto.medicalLicenses = affiliate.medicalLicenses.map((ml) => {
      return ml.unmarshal();
    });
    return affiliateDto;
  }

  public async create(
    dtoAffiliate: UnmarshalledAffiliate,
  ): Promise<UnmarshalledAffiliate> {
    const affiliate = AffiliateMapper.toDomain(dtoAffiliate);
    const inserted = await this.affiliateRepo.create(affiliate);

    /*
    const affiliateTopic = new AffiliateTopic(this.configService);
    await this.messagesService.sendMessage(
      affiliateTopic.topic,
      affiliateTopic.kind,
      {
        affiliateId: inserted.id,
        message: `Affiliate ${
          inserted.id
        } created at ${new Date().toISOString()}`,
      },
    );
    */

    return inserted.unmarshal();
  }

  public async updated(
    dtoAffiliate: UnmarshalledAffiliate,
  ): Promise<UnmarshalledAffiliate> {
    const affiliate = AffiliateMapper.toDomain(dtoAffiliate);
    const affiliateUpdated = await this.affiliateRepo.update(affiliate);
    return affiliateUpdated.unmarshal();
  }

  public async getLicensesByAffiliate(
    affiliateId: string,
  ): Promise<UnmarshalledMedicalLicense[]> {
    const medicalLicenses = await this.affiliateRepo.medicalLicensesByAffiliate(
      affiliateId,
    );

    const medicalLicensesDto = medicalLicenses.map((ml) => {
      return ml.unmarshal();
    });

    return medicalLicensesDto;
  }

  public async findAll(): Promise<UnmarshalledAffiliate[]> {
    const affiliates = await this.affiliateRepo.findAll();

    const affiliatesDto = affiliates.map((aff) => {
      const medicalLicenses = aff.props.medicalLicenses.map((ml) => {
        return {
          id: ml.id,
          initialDate: ml.props.initialDate,
          endDate: ml.props.endDate,
          affiliateId: aff.id,
        };
      });

      return {
        id: aff.id,
        name: aff.props.name,
        medicalLicenses,
      };
    });

    return affiliatesDto;
  }

  public async insertMedicalLicense(
    dtoMedicalLicense: UnmarshalledMedicalLicense,
  ): Promise<UnmarshalledMedicalLicense> {
    const medicalLicense = MedicalLicenseMapper.toDomain(dtoMedicalLicense);
    const inserted = await this.affiliateRepo.insertMedicalLicense(
      medicalLicense,
    );
    return inserted.unmarshal();
  }
}
