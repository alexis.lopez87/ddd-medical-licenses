import {
  CallHandler,
  ExecutionContext,
  HttpException,
  NestInterceptor,
} from '@nestjs/common';
import { catchError, Observable, throwError } from 'rxjs';
import { StatusCodes } from 'http-status-codes';
import {
  ResourceNotFound,
  ValidationError,
} from 'src/medicallicenses/libs/errors';

export class ErrorInterceptors implements NestInterceptor {
  intercept(
    context: ExecutionContext,
    next: CallHandler<any>,
  ): Observable<any> | Promise<Observable<any>> {
    return next.handle().pipe(
      catchError((err) => {
        const request: Request = context.switchToHttp().getRequest();
        //const response: Response = context.switchToHttp().getResponse();

        const message = err?.message || err?.detail || 'Something went wrong';
        const statusCode = this.getStatusCode(err);

        if (statusCode === StatusCodes.INTERNAL_SERVER_ERROR) {
          throw err;
        }

        return throwError(() => {
          return this.createHttpException(request, message, statusCode);
        });
      }),
    );
  }

  getStatusCode(err: any) {
    let statusCode = StatusCodes.INTERNAL_SERVER_ERROR;

    if (err instanceof ValidationError) {
      statusCode = StatusCodes.BAD_REQUEST;
    } else if (err instanceof ResourceNotFound) {
      statusCode = StatusCodes.NOT_FOUND;
    }

    return statusCode;
  }

  createHttpException(request: Request, message: string, statusCode: number) {
    const exception = new HttpException(
      {
        message,
        timestamp: new Date().toISOString(),
        route: request.url,
        method: request.method,
      },
      statusCode,
    );

    return exception;
  }
}
